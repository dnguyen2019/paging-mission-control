const fs = require('fs');
const readline = require('readline');
const timeUtils = require('./timeUtils');

const maxTimeInterval = 5 * 60 * 1000; //5 Minutes in seconds
const alertStatus = class {
    constructor(id, severity, component, timestamp) {
        this.satelliteId = id;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }
}
function processData(filename) {
    let battData = {}; //Dictionary object with satIDs as Key and the last three reported status that is above the threshold
    let tstatData = {}; // only need last three in order to save on memory cost
    let alerts = [];

    var r = readline.createInterface({
        input: fs.createReadStream(filename)
    });

    r.on('line', text => {
        //Read the File Line by Line instead of loading all at once into large memory space
        let x = text.split("|");
        //Parse
        let redHighLimit = parseFloat(x[2]);
        let redLowLimit = parseFloat(x[5]);
        let rawValue = parseFloat(x[6]);
        let satelliteId = parseInt(x[1]);
        let timestamp = x[0];

        //Fill in array if undefined
        if (!battData[satelliteId]) battData[satelliteId] = [];
        if (!tstatData[satelliteId]) tstatData[satelliteId] = [];
        
        //Check Limits and track if condition is violated
        if (redLowLimit > rawValue && x[7] == "BATT") {
            battData[satelliteId].push(timestamp); //add to end
            if (battData[satelliteId].length > 3) battData[satelliteId].shift(); //remove first element
            if (timeUtils.checkTimestamps(battData[satelliteId], maxTimeInterval)) {
                alerts.push(new alertStatus(satelliteId, "RED LOW", "BATT", timeUtils.parseTimestamp(battData[satelliteId][0]))); //report alert occurence
            }
        } else if (redHighLimit < rawValue && x[7] == "TSTAT") {
            tstatData[satelliteId].push(timestamp);
            if (tstatData[satelliteId].length > 3) tstatData[satelliteId].shift();
            if (timeUtils.checkTimestamps(tstatData[satelliteId], maxTimeInterval)) {
                alerts.push(new alertStatus(satelliteId, "RED HIGH ", "TSTAT", timeUtils.parseTimestamp(tstatData[satelliteId][0])));
            }
        }
    });
    r.on('close', obj => console.log(JSON.stringify(alerts))); //output final result once all lines are read
}
//MAIN
//Assumption: Correctly formatted and sorted by time status telemetry data
//Usage: node app.js FILENAME
if (process.argv.length < 3) {
    console.log('Usage: node ' + process.argv[1] + ' FILENAME');
    process.exit(1);
} else {
    processData(process.argv[2]);
}
