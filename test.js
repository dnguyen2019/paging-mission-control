const timeUtils = require('./timeUtils');

test('testing parsing functionality, good input', () => {
    expect(timeUtils.parseTimestamp('20220101 11:56:38.001')).toStrictEqual(new Date('2022-01-01T11:56:38.001Z'));
})

test('testing parsing functionality, bad input', () => {
    expect(timeUtils.parseTimestamp('202asd341 11:56Z3TR8.001')).toEqual(null);
})

test('testing checkTimestamps, close times', () => {
    expect(timeUtils.checkTimestamps(['20180101 23:01:05.001','20180101 23:01:06.001','20180101 23:01:07.001'],300000)).toBe(true);
})
test('testing checkTimestamps, far times', () => {
    expect(timeUtils.checkTimestamps(['20180101 01:01:05.001','20180101 02:01:06.001','20180101 03:01:07.001'],300000)).toBe(false);
})