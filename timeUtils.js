function checkTimestamps(arr, maxTime) {
    if (!arr || arr.length != 3) return false;
    return Math.abs(parseTimestamp(arr[2]) - parseTimestamp(arr[0])) < maxTime
}
function parseTimestamp(ts) {
    let res = new Date();
    res.setUTCFullYear(ts.substring(0, 4), ts.substring(4, 5), ts.substring(7, 8));
    res.setUTCHours(ts.substring(9, 11), ts.substring(12, 14), ts.substring(15, 17), ts.substring(18));
    if (res instanceof Date && !isNaN(res))
        return res;
    else {
        return null;
    }
}
module.exports = {checkTimestamps, parseTimestamp};